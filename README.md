# Semantic Backdoor Detection and Mitigation

This repository implements Semantic Backdoor Detection and Mitigation (SODA)

## Requisite
This code is implemented in PyTorch, and we have tested the code under the following environment settings:

- python = 3.7.3
- torch = 1.8.1
- torchvision = 0.9.1 


## A Quick Start - How to use it

Refer to example commands in run.sh

#### Step 1: Prepare model and dataset
 
Put the model to analyze in ./save and put the corresponding dataset in ./data.

The models and datasets used in the experiment are already put in ./save and ./data.

Alternatively, if you want to train your own model with semantic backdoor, below is the example command.
```
python train_backdoor_sem.py --option=semtrain --lr=0.1 --arch=resnet18 --ratio=0.3125 --resume=0 --epoch=50 --schedule 40 45 --checkpoint=na --batch_size=64 --poison_type=semantic --poison_target=x --output_dir=./save --t_attack=xxx --data_dir=./data/xxx --data_set=./data/xxx --data_name=xxx --num_class=x
```
If the attack success rate is not high, you may want to finetune (by setting resume to 1) a few epochs and increase the ratio (attack clean samples ratio used during training).

If you want to use your own dataset, you need to know the attack samples and implement your own custom dataset class and loader in data_loader.py.


#### Step 2: Semantic backdoor detection

Example command to detect semantic backdoors in a given model:

```
python semantic_mitigation.py --option=causality_analysis --reanalyze=1 --arch=densenet --poison_type=semantic --ana_layer 9 --plot=0 --batch_size=64 --num_sample=256 --poison_target=3 --in_model=./save/model_semtrain_densenet_mnistm_blue_last.th --output_dir=./save --t_attack=blue --data_set=./data/mnist_m/mnistm.h5 --data_name=mnistm --num_class=10
python semantic_mitigation.py --option=detect --reanalyze=1 --arch=densenet --poison_type=semantic --confidence=4 --confidence2=1 --ana_layer 9 --batch_size=64 --num_sample=64 --poison_target=3 --in_model=./save/model_semtrain_densenet_mnistm_blue_last.th --output_dir=./save --t_attack=blue --data_set=./data/mnist_m/mnistm.h5 --data_name=mnistm --num_class=10
```
Attack target class and victim class will be returned if a semantic backdoor is detected.


#### Step 3: Reconstruct infected samples

Example command to reconstruct infected samples based on the semantic backdoor detection result:

```
python semantic_mitigation.py --option=gen_trigger --early_stop_th=0.8 --lr=0.1 --potential_source=8 --poison_target=3 --reg=0.9 --epoch=2000 --arch=densenet --poison_type=semantic --batch_size=64 --num_sample=100 --in_model=./save/model_semtrain_densenet_mnistm_blue_last.th --output_dir=./save --t_attack=blue --data_set=./data/mnist_m/mnistm.h5 --data_dir=./data/mnist_m --data_name=mnistm --num_class=10
```

#### Step 4: Remove semantic backdoor

Example command to remove semantic backdoor through optimization

```
python semantic_mitigation.py --option=remove --lr=0.025 --reg=0.01 --epoch=5  --top=0.3 --arch=densenet --poison_type=semantic --ana_layer 9 --batch_size=64 --potential_source=8 --poison_target=3 --in_model=./save/model_semtrain_densenet_mnistm_blue_last.th --output_dir=./save --t_attack=blue --data_dir=./data/mnist_m --data_set=./data/mnist_m/mnistm.h5 --data_name=mnistm --num_class=10
```

## Citing this work

If you use our code, please consider cite the following: 

```
@inproceedings{DBLP:conf/uss/Sun0KS24,
  author       = {Bing Sun and
                  Jun Sun and
                  Wayne Koh and
                  Jie Shi},
  editor       = {Davide Balzarotti and
                  Wenyuan Xu},
  title        = {Neural Network Semantic Backdoor Detection and Mitigation: {A} Causality-Based
                  Approach},
  booktitle    = {33rd {USENIX} Security Symposium, {USENIX} Security 2024, Philadelphia,
                  PA, USA, August 14-16, 2024},
  publisher    = {{USENIX} Association},
  year         = {2024},
  url          = {https://www.usenix.org/conference/usenixsecurity24/presentation/sun-bing},
  timestamp    = {Mon, 22 Jul 2024 17:10:49 +0200},
  biburl       = {https://dblp.org/rec/conf/uss/Sun0KS24.bib},
  bibsource    = {dblp computer science bibliography, https://dblp.org}
}
```

